# rtrace.io Owncast Theme

this theme is heavily inspired by [Teklynk](https://owncast.teklynk.com/) with some adaptions to match [my blog](https://blog.rtrace.io).
The original Theme from Teklynk came from [here](https://privatebin.teklynk.com/?8cdc821236b512c0#6nmYYv64fnjK7Xgo1sDAXnbyfYqrMGk2SvAHbyqcTcdq).

If you like the theme, feel free to use it, adapt it, extend it.

